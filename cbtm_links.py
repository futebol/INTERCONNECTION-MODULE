import xml.etree.ElementTree as ET
import pickle
import subprocess
import logging

# archive of configuration of ips
# (? all configurations in only one archive ?)

from cbtm_links_config import * ;


""" 
Module to manage the links from the rspec in the Aggregate Manager.
    DOES:
    creates ovs bridges in the server through AM
    creates an archive with the name of the variable 'arq_links' in the AM directory, to comunication
        between the AM and CBTM
    creates the interfaces inside the VMs through the CBTM
    creates ovs bridges with vxlan ports from server to minipc and minipc to server
    

Features:
    connection between VMs in the host server with OpenVSwitch(ovs) bridges
    EXPERIMENTAL = stitching (connection) between ONE VM in the server and ONE VM in another testbed through VXLAN
    EXPERIMENTAL = vxlan connection between the server and minipc (usrp)	


How to use:
In this Module (cbtm_links)
    Add this module in the directory of the aggregate manager
    Change the absolut path inside the global variable 'path_links' (first of the document)
        to point to the AM directory
    Change the variable RACK_USER with the name of the user that can make ssh to the minipcs
    Change the variable testbed_host with the name of the futebol server ( availables: ufrgs, ufmg)

In the Allocate method of the AM,
    insert the function save_links and the correct parameters
In the Delete method of the AM,
    insert the function delete_links and the correct parameters

In the cbtm.py
    add in the header:
        import sys, os
        sys.path.insert(0, "absolute path for cbtm_links.py directory")
        import cbtm_links

    in the method make_vm() of class cbtm(), right before
    the call of the function "template_ctrl.define_and_boot_vm()",
    the xml of the vm must be edited by the InsertLinkInVm function, so

    add the function InsertLinkInVm, with the right parameters, and make the
    return that xml in format string, as in the example below:

    vm_xml = template_ctrl.create_xml(res_num,POOL_PATH+img_name,img_type,br_name)
    vm_xml = cbtm_links.insertLinkInVm(res_num, vm_xml) -> HERE THE XML IS EDITED
    template_ctrl.define_and_boot_vm(self._conn_list,host,vm_xml) 

If the agreggate manager starts as sudo, the keys of ssh to minipcs must be create to allow the commands through
the ssh connection without password. 

For precautions, the public_ips of the testbeds is in outer archive of configuration.


Dependencies:
NEW: the module necessity an python dictionary ( in the follow format) to know the ip address of 
the host where the VM is, to send the commands by ssh to create the bridge and the port vxlan to the server of the CBTM.
Format: 
  key(string) :	value(string)
{ resource_id : ip_address }

if the CBTM has the option that the JFed does not need to send the id of the node in the rspec, this need to be pass
to the save_links as reslist in the format:
list [ list [ string , string, string ]]
[   [ 'node_id' , 'type of image of vm' , 'client id in the rspec' ] ]


UPDATE:
No needs more list( resList ) with the resources list, the module use the rspec to 
    search for the resources_list of the component_manager  testbed.
But if the id of the resource is not in the rspec, the module does not know and break

Creating the functions save_links() and delete_links() as alias of the function manage_link()


the ovs bridges vxlan has the follow format:
    sliceName_resourceId_testbedCodeDestiny
and the ovs bridges 'lan' has the follow format:
    sliceName_linkName

"""

# variables of global configuration :

# name of the archive with the links
arq_links = 'cbtm_links.pickle'

# path of location of this module
path_links = '/opt/f4fam/code/geni-tools/src/gcf_tcd_plugin/'
#path_links = '/home/andrei/PycharmProjects/minicbtm/f4fam/code/geni-tools/src/gcf_tcd_plugin/'

# name of the USER to make access to the hosts
RACK_USER = 'futebol'
# ip of the server
RACK_IP = '192.168.5.1'

# name of the location of the server, use this name like key to access all the dictionaries
testbed_host = 'ufrgs'


# variables to teste
# sobreescrita para TESTE local
# path_links = '/home/andrei/PycharmProjects/FUTEBOL/f4fam/code/geni-tools/src/gcf_tcd_plugin/'

# In the file cbtm_links_config.py are the oficials public ips of the testbeds
# the ips below is only to test
# public_ips = {"ufrgs": "10.10.10.1", "ufmg": "12.12.12.1"}


# variable to find tags in xml
g_rspec_ns = {"rspecV3": "http://www.geni.net/resources/rspec/3"}


def set_log():
    log = logging
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s - %(module)s - %(funcName)s - %(lineno)s - %(message)s')


def make_vni(sliceName):
    """		Create an integer with limit of 16 million to the vnID of VXLAN
            still necessity improvement

    :param sliceName: String, the name of the actual slice

    :return number, an random key base in the sliceName
    """
    word = sliceName
    vni = 0
    for letter in word:
        if letter.isalpha() == True:
            letter = ord(letter.lower()) - 87
        else:
            letter = int(letter)
        vni += letter
    if word[-1].isalpha() == True:
        vni *= (ord(word[-1].lower()) - 87)
    else:
        vni *= 1 + int(word[-1])
    if len(word) > 1:
        if word[-2].isalpha() == True:
            vni *= (ord(word[-2].lower()) - 87)
        else:
            vni *= 1 + int(word[-2])
    if len(word) > 2:
        if word[-3].isalpha() == True:
            vni *= (ord(word[-3].lower()) - 87)
        else:
            vni *= 1 + int(word[-3])
    return vni


def get_linklista():
    # try to open the archive cbtm_links.pickle
    # if not exists, create the archive e try to read again
    try:
        arq = open(path_links + arq_links, "r")
    except:
        arq = open(path_links + arq_links, "w+")
        arq.write(pickle.dumps({}))
        arq.close()
        arq = open(path_links + arq_links, "r")

    # loads the list from the archive
    linklista = pickle.load(arq)
    arq.close()
    return linklista


def manage_link(mode, sliceName, root=None, ip_map=None, resList=None):
    """		Create an archive and the ovs bridges of the links in the rspec

    :param mode: String, 'allocate' or 'delete'. All require
    :param sliceName, String,  the name of the slice name. All require
    :param root: xml.etree.ElementTree.Element, the complete rspec of allocation, require in 'allocate' mode
    :param ip_map: Dict, with { node_id (string) : ip_address_of_the_host_of_vm (string) }
    """

    log = logging.getLogger('geni-delegate')
    log.debug("########## EDITING LINKS ###########")
    #log.debug('Experiment VNI:' + str(make_vni(sliceName)))

    linklista = get_linklista()

    if mode == 'allocate':
        #log.debug("Allocate slice name: " + sliceName)

        # If does not pass the right parameter of the root
        if not isinstance(root, ET.Element):
            if isinstance(root, str):
                # if root is the type str
                log.error("The root is a string. Parsing")
                root = ET.fromstring(root)
                #log.debug(type(root))
            elif isinstance(root, ET.ElementTree):
                # searching for the element root of the tree
                #log.debug("The root is an ElementTree. Searching for the root with getroot")
                root = root.getroot()
            else:
                log.error('The parameter root is an %s and this is not a valid type' %type(root))
                raise Exception('The parameter root is an %s and this is not a valid type ' %type(root))

        # Catch the resources of the testbed_host from rspec
        # and create an list [ with each element an list in the format
        #                       [ resource_id, resource_type, client_id ]   ]
        if resList is None:
            resList = make_resource_list(root)

        # This is to fix the bug where the cbtm_links.pickle is delayed with the delete of the resouces,
        # causing the assign of an resource to an bridge of older experiment
        log.debug('resList %s' % resList)
        log.debug('linkLista: %s' % linklista)

        # in case of error in the delete slice, remove from resList already existent nodes
        log.debug("Bug ?")
        # TODO verify the interfaces in the rasps and delete olds interfaces
        # Cleaning the olds interfaces not deleted
        for resource, img, client in resList:
            # in the machine
            if 'rasp' in img:
                list_br, list_err = call_ssh('pi@'+ip_map[resource], 'sudo ovs-vsctl list-br')
                # list_br = list_br.stdout.read()
                for bridge in list_br.splitlines():
                    out, err = call_ssh('pi@'+ip_map[resource], 'sudo ovs-vsctl --if-exists del-br %s' % bridge)
                    if err != '':
                        log.error("Deleting bridge: %s" % err)

            # in the file
            if resource in linklista:
                log.debug("Delleting %s" % linklista[resource])
                del linklista[resource]
        log.debug('linklista: %s' % linklista)
        log.debug("\n\n");

        ip_address = 0

        links = root.findall('rspecV3:link', g_rspec_ns)
        nodes = root.findall('rspecV3:node', g_rspec_ns)

        # percorre cada um dos links do rspec
        for link in links:
            #log.debug("\n")
            #log.debug("Parsing provided link in the RSpec")

            linkName = ""
            link_type = link.find("rspecV3:link_type", g_rspec_ns)
            # verify if the link has the link_type
            if link_type is None:
                cps = link.findall("rspecV3:component_manager", g_rspec_ns)
                if len(cps) == 2 and cps[0].get("name") != cps[1].get("name"):
                    link_type = 'vxlan'
                elif len(cps) == 1:
                    link_type = 'lan'
                else:
                    log.error('The link does not contain link_type neither 1 or 2 components manager')
                    return None
            else:
                link_type = link_type.get("name")

            log.debug("Link_name: " + link.get("client_id"))
            log.debug("Link_type: " + link_type)

            # interfaces_ref = link.findall('{http://www.geni.net/resources/rspec/3}interface_ref')

            # verificando quais dos nodes no interfaces_ref pertencem ao testbed aonde
            # esta rodando este programa ( testbed_host ), isto e,
            # pega o client_id na interface_ref do link, verifica se o client_id
            # esta no resList, se estiver, cria o link(switch ovs) no servidor
            # e se estiver em um minipc, cria uma vxlan para o minipc e
            # uma vxlan no minipc para o servidor

            interfaces_ref = link.findall('{http://www.geni.net/resources/rspec/3}interface_ref')

            # passando por cada uma das interfaces conectadas ao link
            # for if_el in interfaces_ref:
            for if_el in interfaces_ref:
                interface_node = str(if_el.get('client_id')).split(':')[0]
                #log.debug("LINK - interface ref id: " + interface_node + " : " + str(if_el.attrib))
                #log.debug("Searching for the id in the resList with this name: " + str(interface_node))

                for resource_id, img_type, client_id in resList:

                    #log.debug("comparing: Client_id: " + str(client_id))

                    # comparando o nome do node dentro do link com os nodes do rspec que pertencem ao testbed_host e devem ser alocados
                    if str(client_id) == interface_node:
                        #log.debug("Match of %s" %client_id)
                        if not str(resource_id) in linklista:
                            linklista[str(resource_id)] = []

                        if link_type == 'vxlan':
                            linkName = str(sliceName) + "_" + str(resource_id)

                            # saving the testbed destiny
                            testbed_destiny = ""
                            for comp_man in link.findall('rspecV3:component_manager', g_rspec_ns):
                                # comp_name = comp_man.get("name").split(".")[2]
                                comp_name = comp_man.get("name").split(":")[2]
                                #log.debug("comp_name: " + comp_name)
                                if comp_name != testbed_names[testbed_host]:
                                    for key, value in testbed_names.items():
                                        if value == comp_name:
                                            #log.debug("Component manager destiny + urn")
                                            #log.debug("key->value: " + key +"->"+ value)
                                            testbed_destiny = key

                            # completing the linkName with the code of the testbed_destiny
                            linkName += '_'+testbed_codes[testbed_destiny]

                        elif link_type == 'lan':
                            linkName = sliceName + "_" + link.get("client_id")
                            #log.debug("Adding the name of link in the list")

                        log.debug("img_type: " + str(img_type))
                        log.debug("ip host: " + ip_map[resource_id])

                        final_ip = int(ip_map[resource_id].split(".")[-1])

                        if final_ip > 1:
                            # Creating the local vxlan, between the server and the resource
                            # because the resource is out of the server

                            log.debug("the host with ip " + str(ip_map[resource_id].split(".")[-1]) + " is not the server")

                            log.info("Creating the bridge in the minipc to the server")
                            if 'raspberry' in img_type:
                                host_user = 'pi'  # raspberry
                            else:
                                host_user = RACK_USER  # futebol

                            fullpath_resource = host_user + '@' + ip_map[resource_id]

                            # creating the bridge in the host of the VM
                            create_vxlan(fullpath_resource, RACK_IP, linkName, str(make_vni(sliceName)))

                            log.info("Creating the bridge in the server to the minipc")
                            createOvs = subprocess.call('sudo ovs-vsctl --may-exist add-br ' + linkName + \
                                                        ' && sudo ifconfig ' + linkName + ' up', shell=True)

                            # configuring the port vxlan of bridge in server
                            #                             sudo ovs-vsctl add-port ovs_bridge ovs_port
                            createPort = subprocess.call('sudo ovs-vsctl --may-exist add-port ' + linkName + ' ' + linkName + str(resource_id)+\
                                                         ' -- set interface ' + linkName + str(resource_id)+ ' type=vxlan' +\
                                                         ' options:remote_ip=' + ip_map[resource_id] +\
                                                         ' options:key=' + str(make_vni(sliceName)), shell=True)

                        # searching in the domains for the interface node to view the ip address
                        for node in nodes:
                            log.info("Searching for the ip of the domain")
                            if(node.get("client_id") == client_id):
                                interf = node.find('rspecV3:interface', g_rspec_ns)
                                ip_desc = interf.find('rspecV3:ip', g_rspec_ns)
                                if ip_desc is not None:
                                    ip_address = ip_desc.get("address")
                                else:
                                    ip_address = '0'

                                log.debug(client_id + " IP experiment found: " + ip_address)

                        if ip_address != 0:
                            if resource_id in linklista:
                                linklista[str(resource_id)].append([linkName, ip_address])
                            else:
                                log.error("Where i am?")
                        else:
                            linklista[str(resource_id)].append([linkName])

            # criando as bridges de acordo com o tipo
            if link_type == 'vxlan':
                log.debug("VXLAN")
                log.debug("public_ips:" + str(public_ips))
                # #log.debug("testbed_codes" + str(testbed_codes[testbed_destiny]))
                log.info("Creating VXLAN to testbed_destiny: %r" % testbed_destiny)

                res_id = linkName.split("_")[1]

                log.debug("res_id: " + str(res_id))
                log.debug("resList: " + str(resList))

                log.debug("adding " + str(linkName) + " in linklista")

                if ip_address:
                    linklista[res_id].append( [ linkName, ip_address] )
                else:
                    linklista[res_id].append( linkName )

                ip_destiny = public_ips[testbed_destiny]
                log.debug("ip: " + str(ip_destiny))

                log.info('CREATING OvS bridge ' + str(linkName))
                # This Create an bridge vxlan in the server
                createOvs = subprocess.call('sudo ovs-vsctl --may-exist add-br ' + linkName +\
                                            ' && sudo ifconfig ' + linkName + ' up', shell=True)
                log.debug('setting the stp_enable')
                createOvs = subprocess.call('sudo ovs-vsctl set Bridge ' + linkName +\
                                            " stp_enable=true", shell=True)
                log.debug('adding the port with vxlan')
                createPort = subprocess.call('sudo ovs-vsctl --may-exist add-port ' + linkName + ' vx_' + linkName +\
                                             ' -- set interface vx_' + linkName + ' type=vxlan' +\
                                             ' options:remote_ip=' + ip_destiny +\
                                             ' options:key=' + str(make_vni(sliceName)), shell=True)
            elif link_type == 'lan':
                log.debug("LAN")
                log.info('Creating ovs bridge')
                createOvs = subprocess.call('sudo ovs-vsctl --may-exist add-br ' + linkName +\
                                            ' && sudo ifconfig ' + linkName + ' up', shell=True)

            log.debug("End link")

        log.debug("linklista:" + str(linklista))
        arq = open(path_links + arq_links, "w")
        arq.write(pickle.dumps(linklista))
        arq.close()
    # end of the allocate mode

    if mode == 'delete':
        # delete all the nodes of the slice
        # root is None
        # ip_map is None

        log.info('DELETING the links name from archive links.pic')
        arq = open(path_links + arq_links, "r")
        linklista = pickle.load(arq)

        log.debug("linklista:" + str(linklista))

        # list to delete the resources_id frm the cbtm_links.pickle
        del_list = []
        for res_id in linklista:
            for link in linklista[res_id]:
                log.debug("link: " + str(link))
                log.debug("link: " + str(type(link)))
                log.debug("link: " + str(isinstance(link, list)))
                if isinstance(link, list):
                    log.debug('link %s' % link)
                    link = link[0]

                    log.debug('link %s' % link)
                # deletando todos os links que tem o sliceName no inicio do nome
                if link.split("_")[0] == sliceName:
                    log.info("Deleting ovs " + str(link) + " from Server")
                    subprocess.call('sudo ovs-vsctl --if-exists del-br ' + str(link), shell=True)

                    if not res_id in del_list:
                        del_list.append(res_id)

                    # minipc < 10
                    # 30 <= raspberry <= 42
                    if int(res_id) < 10 \
                    or (29 < int(res_id) < 43)\
                    or ip_map[res_id].split(".")[-1] != '1':
                        if ip_map == None:
                            raise Exception("ip_map == None, was not able to delete the vxlan bridge in minipc")

                        # The resource is minipc (gnuradio)
                        log.info("Deleting " + str(link) + " from minipc " + str(res_id))
                        if int(res_id) < 30 or int(res_id) > 45:
                            host_user = RACK_USER
                        else:
                            host_user = 'pi'
                        fullpath_src = host_user + '@' + ip_map[res_id]
                        delete_vxlan(fullpath_src, str(link))

        # deleting links of linklista from cbtm_links.pickle
        log.debug("del_list: " + str(del_list))
        log.debug("Deleting keys to links from list")
        for del_item in del_list:
            log.debug("del linklista[%s] " + del_item)
            del linklista[del_item]
        log.debug("link_lista: " + str(linklista))

        # writing in the file
        arq = open(path_links + arq_links, "w")
        arq.write(pickle.dumps(linklista))
        arq.close()

    # #log.debug("linklista: " + str(linklista))
    #log.debug("########## FINISHED LINKS ###########\n\n")


# interfaces to facility the use of this module without change who has implement he yet
def save_links(slice_name, root=None, ip_map=None, resList=None):
    """ Interface to facility the use of the manage_link

    :param slice_name: string, the name of the slice
    :param root: xml Element root of the tree of the xml
    :param ip_map: dict with resources -> ip of the host
    :param resList: list of list with the id of the resource, img_type and client_id name
    :return: None
    """
    logging.debug("Saving the links")
    manage_link('allocate', slice_name, root=root, ip_map=ip_map, resList=resList)


def delete_links(slice_name, ip_map=None):
    """ interface to facility the use of the manage_link

    :param slice_name: string, the name of the slice
    :param ip_map: dict with resources -> ip of the host
    :return: None
    """
    logging.debug("Deleting the links")
    manage_link('delete', slice_name, ip_map=ip_map)


def create_vxlan(fullpath_src, ip_dst, linkName, vni):
    ''' Create an ovs bridge with an port vxlan in the host through an ssh command.

    :param fullpath_src:  String, format: user@host_path, host where the bridge ovs and the port vxlan has to be create
    :param ip_dst: String, the ip of the destiny of the vxlan
    :param linkName: String, the name of the link to be the name of the bridge
    :param vni: String, an unique number to be use as key, like an vlan_key
    '''

    # This create a bridge vxlan in the computer to the exterior
    # or in the minipc to the server
    log = logging.getLogger("cbtm_logger")
    log.info("Creating ovs bridge vxlan in minipc")

    # command to create and set the ovs bridge
    COMMAND = 'sudo ovs-vsctl --may-exist add-br ${linkName}' + \
              ' && sudo ifconfig ${linkName} up' + \
              ' && sudo ovs-vsctl set Bridge ${linkName} stp_enable=true ' + \
              ' && sudo ovs-vsctl --may-exist add-port ${linkName} pr_${linkName}  -- set interface pr_${linkName} type=vxlan' + \
              ' options:remote_ip=${ip_destiny}' + \
              ' options:key=' + vni

    COMMAND = COMMAND.replace('${linkName}', linkName)\
                     .replace('${ip_destiny}', ip_dst)

    log.debug("fullpath_src :" + str(fullpath_src))
    log.debug("COMMAND: %r" % COMMAND)

    # sending the command by ssh to the computer
    ssh = subprocess.Popen(['ssh', '-o','UserKnownHostsFile=/dev/null',
                                   '-o', 'StrictHostKeyChecking=no',
                                   '-t', '%s' % fullpath_src,
                            COMMAND], shell=False,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    log.debug(ssh.stdout.read())
    log.debug(ssh.stderr.read())


def delete_vxlan(fullpath_src, linkName):
    """ Delete the ovs bridge linkName from the host through an ssh command

    :param fullpath_src:  String, format: user@host_path, host where the bridge ovs and the port vxlan has to be delete
    :param linkName: String, the name of the link to be the name of the bridge
    """
    log = logging.getLogger("cbtm_logger")
    log.info("Deleting ovs bridge vxlan in minipc or rasp")

    # command to delete the ovs bridge
    COMMAND = 'sudo ovs-vsctl --if-exists del-br '+linkName

    log.debug("fullpath_src :" + str(fullpath_src))
    log.debug("COMMAND: " + str(COMMAND))

    # sending the command by ssh to the computer
    ssh = subprocess.Popen(['ssh', '-o','UserKnownHostsFile=/dev/null',
                                    '-o', 'StrictHostKeyChecking=no',
                                    '-t', '%s' % fullpath_src,
                            COMMAND], shell=False,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    log.debug(ssh.stdout.read().replace("\r", ""))
    log.debug(ssh.stderr.read().replace("\r", ""))


def insertLinkInVm(resource_id, vm_xml):
    """ Read the links in the file cbtm_links.pickle (variable path_links) and insert
    in the xml of VM;

    :param resource_id: string, the id of the resource
    :param vm_xml: string, the xml of the VM in string format

    :return string, xml of the VM
    """

    log = logging.getLogger("cbtm_logger")
    log.info("Connecting the vm in the links")

    # loading the names of the ovs bridges
    arq = open(path_links + arq_links, "r")
    linkslista = pickle.load(arq)
    arq.close()

    root = ET.fromstring(vm_xml)
    devices = root.find('devices')

    log.debug("resource_id: type:" + str(type(resource_id)) + ", value:" + str(resource_id))
    log.debug("linksLista" + str(linkslista))

    # check if the resource_id is in the linklist with the name of bridges
    # where the vm has to be attach
    if (len(linkslista) != 0) and (resource_id in linkslista):
        for link in linkslista[str(resource_id)]:
            # Creating the interface and subattributes and adding in the xml of the VM
            name = link[0]
            ip = link[1] if len(link) > 1 else 0
            log.info("Connecting " + str(resource_id) + " in ovs bridge " + str(name) + ' with ip: ' + ip)
            bridge = ET.Element("interface", attrib={'type': 'bridge'})
            ET.SubElement(bridge, "source", attrib={'bridge': name})
            ET.SubElement(bridge, "model", attrib={'type': 'virtio'})
            ET.SubElement(bridge, "virtualport", attrib={'type': 'openvswitch'})
            ET.SubElement(bridge, "mtu", attrib={'size': '1430'})

            #if ip:
                #ET.SubElement(bridge, 'ip', attrib={'address': ip, 'prefix': '24'})
                #ET.SubElement(bridge, "target", attrib={'dev': })

            devices.append(bridge)

    log.info("Insert of links completed")
    return ET.tostring(root)


def make_resource_list(root):
    """ make an list of the resources of the server

    :param root: xml.etree.ElementTree.Element, the root of the rspec xml tree
    :return: list [ list [ node_id(string), sliver_type(string), client_id(string) ] ]
    """
    log = logging.getLogger("cbtm_logger")
    log.info("Making the resource list")

    res_list = []
    log.debug("Searching for nodes in the rspec")

    # looks for all the nodes in the rspec
    for node in root.findall("rspecV3:node", g_rspec_ns):
        manager = node.get("component_manager_id").split(":")[2]
        log.debug("manager: " + str(manager))
        # if the node belongs to the testbed_host
        if manager == testbed_names[ testbed_host ]:
            node_id = node.get("component_id").split("+")[-1]
            sliver_type = node.find("rspecV3:sliver_type", g_rspec_ns).get("name")
            client_id = node.get("client_id")
            # add informations about it in list
            log.debug("Adding our node to the list")
            res_list.append([node_id, sliver_type, client_id])

    log.debug("returning: res_list: " + str(res_list))
    return res_list


def make_ip_map(RESOURCE_HOST_MAPPING, RACK_IPS, RASP_IPS):
    """ create an dict with the resource_id -> ip_address, to make ssh in the manage_link

    WARNING! function for use only by the testbed of ufrgs

    The source of the informations is the module cbtm_allsettings.py
    however, every testbed has to pass an dict of this type to manage_link

    :param RESOURCE_HOST_MAPPING: dict { resource_id (int) : name_resource (string) }
    :param RACK_IPS: dict { name_resource (string) : ip_address_of_host (string) }
    :param RASP_IPS: dict { name_resrouce (string) : ip_address_of_host (string) }

    :return: dict{ resource_id(string) : ip_address(string) }
    """
    # percorre os dicts from all_cbtm_settings, para criar
    # um dict de resources_id -> ip_address
    logging.info("loading ip_map")

    ip_map = {}
    for key in RESOURCE_HOST_MAPPING:
        name = RESOURCE_HOST_MAPPING[key]
        if name in RACK_IPS:
            logging.debug("name: " + str(name) + " adding ip: " + RACK_IPS[name])
            ip_map.update({ str(key): RACK_IPS[name]} )
        if name in RASP_IPS:
            logging.debug("name: " + str(name) + " adding ip: " + RASP_IPS[name])
            ip_map.update({ str(key): RASP_IPS[name]} )

    logging.debug("ip_map: " + str(ip_map))
    if len(ip_map) != 0:
        logging.debug("ip_map load")
    else:
        logging.error("ip_map empty")

    return ip_map


###########################  Experimental Code #######################################
"""
About the Raspberry Pi with the OS debian 9 (stretch)

I have to disable the daemon dhcpcd and started the networking to the archive localized in 
    '/etc/networks/interfaces'
and the files in the 
    '/etc/networks/interfaces.d/'
can configure the interfaces.

Before disabled the dhcpcd, verify if the configuration of network in the '/etc/networks/interfaces' is correct.
In the UFRGS, the rasps get IP by the dhcp server e the configuration is like below:
    '
    auto lo
    iface lo inet loopback
    
    auto <interface connected in the network>
    iface <interface connected in the network> inet dhcp
    '

I used the command below to switch the service of network:
    '
    sudo systemctl dhcpcd disabled; 
    sudo systemctl networking enabled;
    sudo reboot;
    '
"""


def configure_ips(conn_list, host, res_num, user, vm_ip):
    """
    Set the name and ips of vm's interfaces from ovs bridges

    :param conn_list: (ConnInfo): Libvirt connections to the Racks
    :param host: (string): Host where the VM will be located
    :param res_num:
    :param user:
    :param vm_ip:
    :return: Nothing
    """

    log = logging.getLogger("cbtm_logger")
    log.debug("##### links.py: configure_ips #####")

    log.debug("host: %s" % str(host))
    log.debug("res_num: %s" % str(res_num))
    log.debug("user: %s" % str(user))
    log.debug("vm_ip: %s" % str(vm_ip))

    res_num = str(res_num)

    linklista = get_linklista()
    print linklista

    # para trocar permanentemente o nome de uma interface
    COMMAND = "sudo touch /etc/udev/rules.d/70-persistent-net.rules; "

    # template para setar uma interface com persistencia, mesmo apos o boot
    template = "auto %s\n" \
               "iface %s inet static \n" + \
               "address %s \n" + \
               "netmask 255.255.255.0\n" \
               "mtu 1430\n\n"

    if user == 'pi':
        # The resource is an Raspberry
        if host is None:
            host = vm_ip

        name_macs = get_name_macs(user, vm_ip)
        log.debug("name_macs: %s\n" % name_macs)

        # delete old interfaces in the raspberry
        call_ssh(user + '@' + vm_ip, 'sudo rm /etc/network/interfaces.d/*')

        # go through the details of each connection
        for sub_lista in linklista[str(res_num)]:
            if_name = sub_lista[0]  # interface name
            ip_local = sub_lista[1]  # ip of the interface

            log.debug("sub_lista: %s" % sub_lista)
            log.debug("name_macs: %s" % name_macs)

            # search for the name of the interface from the file in the resource and compare directed with the name.
            for if_host, mac in name_macs.items():
                if if_host == if_name:
                    log.debug("Interface %s found" % if_name)

                    # save configurations of the interface created
                    COMMAND += 'sudo touch /etc/network/interfaces.d/%s.config; ' % if_name
                    COMMAND += 'echo \'%s\' | sudo tee -a /etc/network/interfaces.d/%s.config; ' \
                                % (template, if_name) % (if_name, if_name, ip_local)

                    COMMAND += 'sudo ifconfig %s %s; ' % (if_name, ip_local)
                    COMMAND += 'sudo ifconfig %s mtu 1430; ' % if_name
    else:
        # The resource is a VM
        try:
            conn = conn_list[host].conn
        except Exception as e:
            log.error('Could not find connection for the intended rack:', host)
            raise

        dom = conn.lookupByName("basic_eu_" + res_num)
        dom_xml = dom.XMLDesc(0)
        root = ET.fromstring(dom_xml)
        # log.debug("name: " + root.find("name").get)

        # this macs is from inside the vm, to future change the name of the interface
        # the mac is the only thing consistent in and out of the VM
        name_macs = get_name_macs(user, vm_ip)
        # log.debug("linklista: " + str(linklista))

        if not str(res_num) in linklista:
            # log.debug("There is no has interfaces to change")
            return ''

        for sub_lista in linklista[str(res_num)]:
            if_name = sub_lista[0]
            ip_local = sub_lista[1]
            # log.debug("%r -> %r" % (if_name, ip_local))
            for interface in root.iter('interface'):

                # searching in the interface details of the xml of vm
                # if the interface is of the type bridge and the source of the bridge of the interface
                # is the same are the name of the bridge in the linklista, to change the interface name
                bridge_source = ''
                if interface.attrib['type'] == 'bridge':
                    bridge_source = interface.find('source').attrib['bridge']

                # log.debug("bridge_source: %r == if_name %r " % (bridge_source, if_name))
                if interface.attrib['type'] == 'bridge' and bridge_source == if_name:
                    log.info("Procurando macs nas interfaces bridge no xml")
                    out_mac = interface.find('mac').attrib['address']

                    for old_name, mac in name_macs.items():
                        # log.debug("MAC: %r ?= out: %r" % (mac, out_mac))
                        old_name = str(old_name)

                        # finding the default name of the interface
                        if out_mac == mac:
                            # log.debug("Switching the name from %r to %r" %(old_name, if_name))

                            COMMAND += 'echo \'SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ' + \
                                       'ATTR{address}=="' + out_mac + '", ATTR{dev_id}=="0x0", ' + \
                                       'ATTR{type}=="1", NAME="' + if_name + '"\' | sudo tee -a /etc/udev/rules.d/70-persistent-net.rules; '

                            # log.debug("Setting the ip of the interface")

                            COMMAND += 'sudo touch /etc/network/interfaces.d/%s.config; ' % if_name
                            COMMAND += 'echo \'%s\' | sudo tee -a /etc/network/interfaces.d/%s.config; ' % (template, if_name) % (if_name, if_name, ip_local)

                            COMMAND += 'sudo ifconfig %s %s; ' % (old_name, ip_local)
                            COMMAND += 'sudo ifconfig %s mtu 1430; ' % old_name
                            COMMAND += 'sudo reboot; '

    if COMMAND != '':
        # sending the COMMAND to the resource
        fullpath = str(user) + '@' + str(vm_ip)
        log.info("Configuring the resourve: %r" % fullpath)

        log.debug("COMMAND: %r" % COMMAND)

        ssh_out, ssh_err = call_ssh(fullpath, COMMAND)
        log.debug("out %r" % ssh_out)
        log.debug("err: %r" % ssh_err)

        # log.debug("out %r" % ssh.stdout.read().replace("\r", ""))
        # log.debug("err: %r" % ssh.stderr.read().replace("\r", ""))

    log.debug("### End of configure ips ###")


def call_ssh(fullpath, COMMAND):
    """
    Send an COMMAND by the network to the user@ip give as fullpath and return an tuple of strings with
    the messages of out and error.

    :param fullpath: String, in the format 'user@ip_address'
    :param COMMAND: String, command or serie of command to be send
    :return: "tuple" ( "string", "string"): ( stdout.read(),  stderr.read() )
    """

    ssh_res = subprocess.Popen(['ssh', '-o','UserKnownHostsFile=/dev/null',
                                '-o', 'StrictHostKeyChecking=no',
                                '-t', '%s' % fullpath,
                                COMMAND], shell=False,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    return ssh_res.stdout.read(), ssh_res.stderr.read()


def debug_ssh(ssh):
    log = logging.getLogger("cbtm_logger")
    #log.debug("out " + str(ssh.stdout.read()))
    #log.debug("err: " + str(ssh.stderr.read()))


def get_name_macs(user, out_ip):
    """
    Get the interfaces and macs of one resource, using the command 'ip link' and
    return an dict with the respective name of interfaces and mac of the interface

    :param user: String, the user of the resource
    :param out_ip: String, the ip of control of the resource
    :return: dict[ String: name of the interface ] = String: mac of the interface
    """
    log = logging.getLogger("cbtm_logger")

    ssh_out, ssh_err = call_ssh(user + '@'+ out_ip, 'ip link')
    # ssh_out = ssh.stdout.read().replace("\r", "")
    # ssh_err = ssh.stderr.read().replace("\r", "")

    if_name = ''
    name_macs = {}

    for line in ssh_out.splitlines():
        mac = line.split('link/')
        if len(mac) == 1:
            fields = line.split(": ")

            log.debug('fields: %s[%d]' % (fields, len(fields)))

            if_name = fields[1]
            name_macs[if_name] = ''
        else:
            name_macs[if_name] = mac[1].split(" ")[1]

    log.debug(name_macs)
    return name_macs


def parse_name_mac(ifconfig):
    """
    Parsing an return of ifconfig searching for the name of interfaces and his respectives macs
    return an dictionary with the keys the name of the interface and value the mac

    The old method, is not utilized more because it cause problems in some Raspberry

    :param ifconfig: string with the return of 'ifconfig | grep HWaddr'
    :return: interface_name : mac_address ( dict )
    """
    log = logging.getLogger("cbtm_logger")

    #log.debug("\n%s"% ifconfig)
    dict = {}
    for lines in ifconfig.split('\n'):
        #log.debug("line: " + lines)
        words = lines.split(' ')

        name = words[0]
        for w in words:
            #log.debug("w: " + str(w), )
            if w == 'HWaddr':
                index = words.index(w)
                mac_addr = words[index+1]
                #log.debug('mac: %S' % mac_addr)
                dict[ name ] = mac_addr
    return dict
