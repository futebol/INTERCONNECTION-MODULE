Module to manage the links from the rspec in the Aggregate Manager.
    DOES:
    creates ovs bridges in the server through AM
    creates an archive with the name of the variable 'arq_links' in the AM directory, to comunication
        between the AM and CBTM
    creates the interfaces inside the VMs through the CBTM
    creates ovs bridges with vxlan ports from server to minipc and minipc to server
    

Features:
    connection between VMs in the host server with OpenVSwitch(ovs) bridges
    EXPERIMENTAL = stitching (connection) between ONE VM in the server and ONE VM in another testbed through VXLAN
    EXPERIMENTAL = vxlan connection between the server and minipc (usrp)	


How to use:
In this Module (cbtm_links)
    Add this module in the directory of the aggregate manager
    Change the absolut path inside the global variable 'path_links' (first of the document)
        to point to the AM directory
    Change the variable RACK_USER with the name of the user that can make ssh to the minipcs
    Change the variable testbed_host with the name of the futebol server ( availables: ufrgs, ufmg)

In the Allocate method of the AM,
    insert the function save_links and the correct parameters
In the Delete method of the AM,
    insert the function delete_links and the correct parameters

In the cbtm.py
    add in the header:
        import sys, os
        sys.path.insert(0, "absolute path for cbtm_links.py directory")
        import cbtm_links

    in the method make_vm() of class cbtm(), right before
    the call of the function "template_ctrl.define_and_boot_vm()",
    the xml of the vm must be edited by the InsertLinkInVm function, so

    add the function InsertLinkInVm, with the right parameters, and make the
    return that xml in format string, as in the example below:

    vm_xml = template_ctrl.create_xml(res_num,POOL_PATH+img_name,img_type,br_name)
    vm_xml = cbtm_links.insertLinkInVm(res_num, vm_xml) -> HERE THE XML IS EDITED
    template_ctrl.define_and_boot_vm(self._conn_list,host,vm_xml) 

If the agreggate manager starts as sudo, the keys of ssh to minipcs must be create to allow the commands through
the ssh connection without password. 

For precautions, the public_ips of the testbeds is in outer archive of configuration.


Dependencies:
NEW: the module necessity an python dictionary ( in the follow format) to know the ip address of 
the host where the VM is, to send the commands by ssh to create the bridge and the port vxlan to the server of the CBTM.
Format: 
  key(string) :	value(string)
{ resource_id : ip_address }

if the CBTM has the option that the JFed does not need to send the id of the node in the rspec, this need to be pass
to the save_links as reslist in the format:
list [ list [ string , string, string ]]
[   [ 'node_id' , 'type of image of vm' , 'client id in the rspec' ] ]


UPDATE:
No needs more list( resList ) with the resources list, the module use the rspec to 
    search for the resources_list of the component_manager  testbed.
But if the id of the resource is not in the rspec, the module does not know and break

Creating the functions save_links() and delete_links() as alias of the function manage_link()


the ovs bridges vxlan has the follow format:
    sliceName_resourceId_testbedCodeDestiny
and the ovs bridges 'lan' has the follow format:
    sliceName_linkName
