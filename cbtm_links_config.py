# Arquivo de configuracao with the public ips of the islands of the FUTEBOL
# to be used by the cbtm_links.py

# codes of the testbeds to be use in the name of the bridge with vxlan
testbed_codes = {'example1': '1',
                 'example2': '2',
                 }

# IDN publics of the testbeds in the rspec. use to parsing the rspec
testbed_names = { 'example1':  'IDN+example1+authority+am',
                  'example2':  'IDN+example2+authority+am',
                  }

# public ips of the testbeds
public_ips = {
        'example1': '192.0.2.0/24 ',
        'example2': '192.0.2.1/24 ',
        }
